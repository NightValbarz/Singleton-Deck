package application;

import workflow.Deck;

public class App {

	public static void main(String[] args) {
		Deck deck = Deck.getInstance();
		deck.print();
	}

}
